import numpy as np


def calc_ndvi(nir: np.ndarray, red: np.ndarray):
    return (nir - red) / (nir + red)


def calc_ndwi(nir: np.ndarray, green: np.ndarray):
    return (green - nir) / (green + nir)


def calc_lswi(nir: np.ndarray, swir: np.ndarray):
    return (nir - swir) / (nir + swir)


def calc_arvi(nir: np.ndarray, red: np.ndarray, blue: np.ndarray):
    return (nir - 2 * red + blue) / (nir + 2 * red + blue)


def calc_msavi2(nir: np.ndarray, red: np.ndarray):
    return (2 * nir + 1 - np.sqrt(np.square(2 * nir + 1)) - 8 * (nir - red)) / 2


def calc_mtvi2(nir: np.ndarray, red: np.ndarray, green: np.ndarray):
    return (1.5 * (1.2 * (nir - green) - 2.5 * (red - green))) / (
                np.sqrt(np.square(2 * nir + 1) - (6 * nir - 5 * np.sqrt(red))) - 0.5)


def calc_vari(red: np.ndarray, green: np.ndarray, blue: np.ndarray):
    return (green - red) / (green + red - blue)


def calc_tgi(red: np.ndarray, green: np.ndarray, blue: np.ndarray):
    return (120.0 * (red - blue) - 190.0 * (red - green)) / 2


def calc_lst(k1: float, k2: float, toa_radiance: np.ndarray):
    return k2 / np.log((k1 / toa_radiance) + 1)


def calc_vci(ndvi: np.ndarray):
    return (ndvi - ndvi.min()) / (ndvi.max() - ndvi.min())


def calc_mndwi(green: np.ndarray, swir: np.ndarray):
    return (green - swir) / (green + swir)


def calc_wri(red: np.ndarray, green: np.ndarray, nir: np.ndarray, swir: np.ndarray):
    return (green + red) / (nir + swir)


def calc_ndti(red: np.ndarray, green: np.ndarray):
    return (red - green) / (red + green)


def calc_awei(green: np.ndarray, nir: np.ndarray, swir1: np.ndarray, swir2: np.ndarray):
    return 4 * (green - swir2 - 0.25 * nir + 2.75 * swir1)


def calc_osi(red: np.ndarray, green: np.ndarray, blue: np.ndarray):
    return (red + green) / blue


def calc_nbr(nir: np.ndarray, swir: np.ndarray):
    return (nir - swir) / (nir + swir)
